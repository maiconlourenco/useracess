import React from "react";
import { useParams, Link, Redirect } from "react-router-dom";
import { getMember } from "../../database/members";

const Company = () => {
  const params = useParams();
  const member = getMember(params.id);

  if (member.type !== "pj") {
    return <Redirect to="/not-found" />;
  }

  return (
    <div>
      <h1>Detalhes da Empresa</h1>
      <Link to="/">Voltar</Link>
      <p>Nome da Empresa: {member && member.name}</p>
    </div>
  );
};

export default Company;
