import React from "react";
import { useParams, Link, Redirect } from "react-router-dom";
import { getMember } from "../../database/members";

const Customer = () => {
  const params = useParams();
  const member = getMember(params.id);

  if (member.type !== "pf") {
    return <Redirect to="/not-found" />;
  }

  return (
    <div>
      <h1>Detalhes do cliente</h1>
      <Link to="/">Voltar</Link>
      <p>Nome: {member && member.name}</p>
    </div>
  );
};

export default Customer;
