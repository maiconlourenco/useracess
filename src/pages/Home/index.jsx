import React, { useState } from "react";
import Links from "../../components/Links";
import { members } from "../../database/members";
import UserForm from "../../components/Form";
import "./styles.css";

const HomePage = () => {
  const [membersList, setMembersList] = useState([...members]);

  const handleChange = (list) => {
    setMembersList(list);
  };

  return (
    <div>
      <UserForm onSubmit={handleChange} />
      <Links members={membersList} />
    </div>
  );
};

export default HomePage;
