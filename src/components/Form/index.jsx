import React, { useState } from "react";
import { members, addMember } from "../../database/members";

import "./styles.css";

const UserForm = (props) => {
  const [membersList, setMembersList] = useState([...members]);
  const [nome, setNome] = useState("");
  const [tipo, setTipo] = useState("");

  const submitMember = (e) => {
    e.preventDefault();
    const newMember = {
      id: `${parseInt(members[members.length - 1].id) + 1}`,
      name: nome,
      type: tipo,
    };
    addMember(newMember);
    setMembersList((member) => [...members]);
    props.onSubmit(setMembersList);
  };

  const handleChange = (event) => {
    console.log(event);

    setTipo(event.target.value);
  };

  return (
    <div>
      <form className="form-container" onSubmit={submitMember}>
        <h1>Bem-Vindo!</h1>

        <div>
          <label>Nome: </label>
          <input
            onChange={(e) => setNome(e.target.value)}
            value={nome}
            placeholder="Cliente/Empresa"
          />
        </div>
        <div className="select-block">
          <label>Tipo: </label>
          <select name="select" value={tipo} onChange={handleChange}>
            <option value="" disabled hidden>
              Select an option
            </option>
            <option value="pf">Pessoa Física</option>
            <option value="pj">Pessoa Jurídica</option>
          </select>
        </div>

        <button type="submit">Adicionar Membro</button>
      </form>
    </div>
  );
};

export default UserForm;
