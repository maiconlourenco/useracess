import React from "react";
import { Link } from "react-router-dom";
import { members } from "../../database/members";
import "./styles.css";

const Links = (props) => {
  const memberList = props.members ? props.members : members;
  return (
    <div>
      {memberList.map(({ id, type, name }, index) => (
        <div key={index} className="page-link">
          <Link
            style={{ textDecoration: "none" }}
            to={`/${type === "pf" ? "customer" : "company"}/${id}`}
          >
            {name}
          </Link>
        </div>
      ))}
    </div>
  );
};

export default Links;
