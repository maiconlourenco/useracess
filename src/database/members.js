export const members = [
  { id: "1", name: "Hidan", type: "pf" },
  { id: "2", name: "Jack", type: "pf" },
  { id: "3", name: "Kenzie", type: "pj" },
  { id: "4", name: "Google", type: "pj" },
  { id: "5", name: "Facebook", type: "pj" },
];

export const getMember = (memberId) => {
  return members.find(({ id }) => id === memberId);
};

export const addMember = (member) => {
  members.push(member);
};
