import React from "react";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import Customer from "./pages/Customer";
import Company from "./pages/Company";
import HomePage from "./pages/Home";
import NotFound from "./pages/NotFound";

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route path="/customer/:id">
            <Customer />
          </Route>

          <Route path="/company/:id">
            <Company />
          </Route>

          <Route path="/not-found">
            <NotFound />
          </Route>

          <Route path="/">
            <HomePage />
          </Route>
        </Switch>
      </header>
    </div>
  );
};

export default App;
